#!/bin/sh

export CLASSNAME=com.rathravane.clerk.tool.ClerkTool

# use JAVA_HOME if provided
if [ -z $JAVA_HOME ]; then
	JAVA=java
else
	JAVA=$JAVA_HOME/bin/java
fi

# this option prevents a slow start on JRE 1.7 when we're low on entropy
export JAVA_OPTS=-Djava.security.egd=file:/dev/./urandom

# run java. The classpath is the etc dir for config files, and the lib dir
# for all the jars.
$JAVA $JAVA_OPTS -cp etc:lib/* $CLASSNAME $*
