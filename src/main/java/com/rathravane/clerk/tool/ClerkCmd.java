package com.rathravane.clerk.tool;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Vector;

import com.rathravane.clerk.IamDb;
import com.rathravane.till.console.CmdLinePrefs;
import com.rathravane.till.console.ConsoleProgram.usageException;
import com.rathravane.till.console.shell.consoleLooper.inResult;
import com.rathravane.till.console.shell.simpleCommand;
import com.rathravane.till.nv.rrNvReadable.missingReqdSetting;

public abstract class ClerkCmd extends simpleCommand
{
	protected ClerkCmd ( String name, boolean reqDb )
	{
		super ( name );
		fReqDb = reqDb;
	}

	@Override
	protected final inResult execute ( HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
		throws usageException, missingReqdSetting
	{
		final IamDb<?,?> db = getDb ( workspace );
		if ( fReqDb && db == null )
		{
			outTo.println ( "Use connect to connect to a clerk DB" );
			return inResult.kReady;
		}

		final Vector<String> args = p.getFileArguments ();
		execute ( db, args, workspace, p, outTo );

		return inResult.kReady;
	}

	protected abstract void execute ( IamDb<?,?> db, Vector<String> args,
		HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
			throws usageException, missingReqdSetting;
	
	private final boolean fReqDb;

	private IamDb<?,?> getDb ( HashMap<String,Object> workspace )
	{
		final Object o = workspace.get ( "iamDb" );
		if ( o instanceof IamDb )
		{
			return (IamDb<?,?>) o;
		}
		return null;
	}
}
