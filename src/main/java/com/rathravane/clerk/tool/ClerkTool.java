package com.rathravane.clerk.tool;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import com.rathravane.clerk.IamDb;
import com.rathravane.clerk.access.AccessControlList;
import com.rathravane.clerk.access.AclUpdateListener;
import com.rathravane.clerk.access.Resource;
import com.rathravane.clerk.exceptions.IamGroupExists;
import com.rathravane.clerk.exceptions.IamIdentityDoesNotExist;
import com.rathravane.clerk.exceptions.IamIdentityExists;
import com.rathravane.clerk.exceptions.IamSvcException;
import com.rathravane.clerk.identity.ApiKey;
import com.rathravane.clerk.identity.Group;
import com.rathravane.clerk.identity.Identity;
import com.rathravane.clerk.impl.common.CommonJsonDb.AclFactory;
import com.rathravane.clerk.impl.s3.S3IamDb;
import com.rathravane.till.console.CmdLinePrefs;
import com.rathravane.till.console.ConsoleProgram;
import com.rathravane.till.console.shell.consoleLooper;
import com.rathravane.till.console.shell.stdCommandList;
import com.rathravane.till.nv.rrNvReadable;
import com.rathravane.till.nv.rrNvReadable.missingReqdSetting;

public class ClerkTool extends ConsoleProgram
{
	public ClerkTool ()
	{
		fDb = null;
	}

	@Override
	protected looper init ( rrNvReadable p, CmdLinePrefs cmdLine ) throws rrNvReadable.missingReqdSetting, rrNvReadable.invalidSettingValue, startupFailureException
	{
		return new consoleLooper (
			new String[] { "clerk tool" },
			"> ",
			". ",
			new commandSet ()
		);
	}

	public static void main ( String[] args ) throws Exception
	{
		new ClerkTool().runFromMain ( args );
	}

	private S3IamDb fDb;

	private class ClerkAclFactory implements AclFactory
	{
		@Override
		public AccessControlList createDefaultAcl ( AclUpdateListener acll )
		{
			return new AccessControlList ( acll );
		}
	}
	
	private class commandSet extends stdCommandList 
	{
		public commandSet ()
		{
			registerCommand ( new ClerkCmd ( "connect", false )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 4 )
					{
						outTo.println ( "usage: connect <apiKey> <secretKey> <bucket> <prefix>" );
						return;
					}

					if ( fDb != null ) fDb.close ();
					
					try
					{
						fDb = new S3IamDb.Builder ()
							.withAccessKey ( args.elementAt ( 0 ) )
							.withSecretKey ( args.elementAt ( 1 ) )
							.withBucket ( args.elementAt ( 2 ) )
							.withPathPrefix ( args.elementAt ( 3 ) )
							.usingAclFactory ( new ClerkAclFactory () )
							.build ();
						workspace.put ( "iamDb", fDb );
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "createUser", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 1 )
					{
						outTo.println ( "usage: createUser <userId>" );
						return;
					}

					try
					{
						fDb.createUser ( args.elementAt(0) );
						outTo.println ( "ok." );
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
					catch ( IamIdentityExists e )
					{
						outTo.println ( "The identity already exists." );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "enableUser", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 1 )
					{
						outTo.println ( "usage: enableUser <userId>" );
						return;
					}

					try
					{
						final Identity i = fDb.loadUser ( args.elementAt(0) );
						if ( i != null )
						{
							i.enable ( true );
							outTo.println ( "ok." );
						}
						else
						{
							outTo.println ( "no such user." );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "disableUser", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 1 )
					{
						outTo.println ( "usage: disableUser <userId>" );
						return;
					}

					try
					{
						final Identity i = fDb.loadUser ( args.elementAt(0) );
						if ( i != null )
						{
							i.enable ( false );
							outTo.println ( "ok." );
						}
						else
						{
							outTo.println ( "no such user." );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "createGroup", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () < 1 || args.size () > 2 )
					{
						outTo.println ( "usage: createGroup [<groupId>] <groupName>" );
						return;
					}

					try
					{
						if ( args.size () == 1 )
						{
							final Group g = fDb.createGroup ( args.elementAt(0) );
							outTo.println ( "Group " + g.getId () + " created." );
						}
						else
						{
							final Group g = fDb.createGroup ( args.elementAt (0), args.elementAt(1) );
							outTo.println ( "Group " + g.getId () + " created." );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
					catch ( IamGroupExists e )
					{
						outTo.println ( "Group exists: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "addToGroup", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 2 )
					{
						outTo.println ( "usage: addToGroup <userId> <groupId>" );
						return;
					}

					try
					{
						fDb.addUserToGroup ( args.elementAt(1), args.elementAt(0) );
						outTo.println ( "ok." );
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
					catch ( IamIdentityDoesNotExist e )
					{
						outTo.println ( e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "removeFromGroup", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 2 )
					{
						outTo.println ( "usage: removeFromGroup <userId> <groupId>" );
						return;
					}

					try
					{
						fDb.removeUserFromGroup ( args.elementAt(1), args.elementAt(0) );
						outTo.println ( "ok." );
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
					catch ( IamIdentityDoesNotExist e )
					{
						outTo.println ( e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "listGroups", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 1 )
					{
						outTo.println ( "usage: listGroups <userId>" );
						return;
					}

					try
					{
						for ( String user : fDb.getUsersGroups ( args.elementAt ( 0 ) ) )
						{
							outTo.println ( user );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
					catch ( IamIdentityDoesNotExist e )
					{
						outTo.println ( e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "listUsers", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 0 )
					{
						outTo.println ( "usage: listUsers" );
						return;
					}

					try
					{
						for ( String user : fDb.getAllUsers () )
						{
							outTo.println ( user );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "findUsers", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 1 )
					{
						outTo.println ( "usage: findUsers <startingWith>" );
						return;
					}

					try
					{
						for ( String user : fDb.findUsers ( args.elementAt ( 0 ) ) )
						{
							outTo.println ( user );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "setData", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 3 )
					{
						outTo.println ( "usage: setData <userId> <name> <value>" );
						return;
					}

					try
					{
						final Identity i = fDb.loadUser ( args.elementAt ( 0 ) );
						if ( i != null )
						{
							i.putUserData ( args.elementAt ( 1 ), args.elementAt ( 2 ) );
							outTo.println ( "ok." );
						}
						else
						{
							outTo.println ( "Couldn't find user." );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "clearData", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 2 )
					{
						outTo.println ( "usage: clearData <userId> <name>" );
						return;
					}

					try
					{
						final Identity i = fDb.loadUser ( args.elementAt ( 0 ) );
						if ( i != null )
						{
							i.removeUserData ( args.elementAt ( 1 ) );
							outTo.println ( "ok." );
						}
						else
						{
							outTo.println ( "Couldn't find user." );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "setPassword", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 2 )
					{
						outTo.println ( "usage: setPassword <userId> <password>" );
						return;
					}

					try
					{
						final Identity i = fDb.loadUser ( args.elementAt ( 0 ) );
						if ( i != null )
						{
							i.setPassword ( args.elementAt ( 1 ) );
							outTo.println ( "ok." );
						}
						else
						{
							outTo.println ( "Couldn't find user." );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "createApiKey", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 1 )
					{
						outTo.println ( "usage: createApiKey <userId>" );
						return;
					}

					try
					{
						final Identity i = fDb.loadUser ( args.elementAt ( 0 ) );
						if ( i != null )
						{
							final ApiKey key = i.createApiKey ();
							outTo.println ( "   key: " + key.getKey () );
							outTo.println ( "secret: " + key.getSecret () );
						}
						else
						{
							outTo.println ( "Couldn't find user." );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "getUser", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 1 )
					{
						outTo.println ( "usage: getUser <userId>" );
						return;
					}

					try
					{
						final Identity i = fDb.loadUser ( args.elementAt ( 0 ) );
						if ( i != null )
						{
							outTo.println ( "Enabled: " + i.isEnabled () );

							outTo.println ( "Data" );
							final Map<String,String> data = i.getAllUserData ();
							for ( Entry<String,String> e : data.entrySet () )
							{
								outTo.println ( "\t" + e.getKey() + ": " + e.getValue () );
							}

							outTo.println ( "Groups" );
							for ( String group : i.getGroupIds () )
							{
								outTo.println ( "\t" + group );
							}
						}
						else
						{
							outTo.println ( "Couldn't find user." );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "listAcl", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 1 )
					{
						outTo.println ( "usage: listAcl <resourceId>" );
						return;
					}

					try
					{
						final String resName = args.elementAt ( 0 );
						final AccessControlList acl = fDb.getAclFor ( new Resource ()
							{
								@Override public String getId () { return resName; }
							}
						);

						if ( acl == null )
						{
							outTo.println ( "No ACL for " + resName );
						}
						else
						{
							outTo.println ( acl.asJson ().toString ( 4 ) );
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "grant", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 3 )
					{
						outTo.println ( "usage: grant <resource> <userOrGroupId> READ|WRITE" );
						return;
					}

					final String resource = args.elementAt ( 0 );
					final String userOrGroup = args.elementAt ( 1 );
					final String access = args.elementAt ( 2 );
					if ( !access.equals ( "READ" ) && !access.equals ( "WRITE" ) )
					{
						outTo.println ( "access must be READ or WRITE" );
						return;
					}

					try
					{
						final AccessControlList acl = fDb.getAclFor ( new Resource ()
						{
							@Override
							public String getId ()
							{
								return resource;
							}
						} );
						
						final Identity i = fDb.loadUser ( userOrGroup );
						if ( i != null )
						{
							acl.permit ( userOrGroup, access );
						}
						else
						{
							final Group g = fDb.loadGroup ( userOrGroup );
							if ( g != null )
							{
								acl.permit ( userOrGroup, access );
							}
							else
							{
								outTo.println ( "No user or group named '" + userOrGroup + "' was found." );
							}
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "revoke", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 3 )
					{
						outTo.println ( "usage: revoke <resource> <userOrGroupId> READ|WRITE" );
						return;
					}

					final String resource = args.elementAt ( 0 );
					final String userOrGroup = args.elementAt ( 1 );
					final String access = args.elementAt ( 2 );
					if ( !access.equals ( "READ" ) && !access.equals ( "WRITE" ) )
					{
						outTo.println ( "access must be READ or WRITE" );
						return;
					}

					try
					{
						final AccessControlList acl = fDb.getAclFor ( new Resource ()
						{
							@Override
							public String getId ()
							{
								return resource;
							}
						} );
						
						final Identity i = fDb.loadUser ( userOrGroup );
						if ( i != null )
						{
							acl.clear ( userOrGroup, access );
						}
						else
						{
							final Group g = fDb.loadGroup ( userOrGroup );
							if ( g != null )
							{
								acl.clear ( userOrGroup, access );
							}
							else
							{
								outTo.println ( "No user or group named '" + userOrGroup + "' was found." );
							}
						}
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );

			registerCommand ( new ClerkCmd ( "sweepExpiredTags", true )
			{
				@Override
				protected void execute ( IamDb<?,?> db, Vector<String> args,
					HashMap<String,Object> workspace, CmdLinePrefs p, PrintStream outTo )
						throws usageException, missingReqdSetting
				{
					if ( args.size () != 0 )
					{
						outTo.println ( "usage: sweepExpiredKeys" );
						return;
					}

					try
					{
						fDb.sweepExpiredTags ();
					}
					catch ( IamSvcException e )
					{
						outTo.println ( "Problem connecting to S3: " + e.getMessage () );
					}
				}
			} );
		}
	}
}
